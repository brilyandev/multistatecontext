import React, {createContext, useContext, useReducer, useState} from 'react';
import logo from './logo.svg';
import './App.css';

const reducer = (state, action) => {
  switch (action.type) {
    case 'increment':
      return { count: state.count + 1 };
    case 'decrement':
      return { count: state.count - 1 };  
    case 'change':
      return { profile: action.text };
    case 'data':
      return { data: [...state.data,action.data] };  
    case 'data2':
      return { data2: [...state.data2,action.data] };  
    default:
      return state;
  }
};

function Counter(){
  var {state, dispatch} = useContext(StateContext)
  //const {state, dispatch} = useContext(StateProvider)//useReducer(reducer, { count: 0 });
  var [textdata, setTextData] = useState("")
  return(
    <>
      <label>Syahri Ramadhan {state.count}</label>
      <label>Profile {state.profile}</label>
      <label>Data {JSON.stringify(state.data)}</label>
      <label>Data2 {JSON.stringify(state.data2)}</label>
      <button onClick={()=>dispatch({type:'increment'})}>Increment</button>
      <button onClick={()=>dispatch({type:'decrement'})}>Decrement</button>
      <input value={textdata} onChange={(e)=>setTextData(e.target.value)} />
      <button onClick={()=>dispatch({type:'change', text:textdata})}>Change Text</button>
      <button onClick={()=>dispatch({type:'data', data:{'Nama': textdata}})}>Add Data</button>
      <button onClick={()=>dispatch({type:'data2', data:{'Nama': textdata}})}>Add Data</button>
    </>
  )
}

function ViewCounter(){
  var {state, dispatch} = useContext(StateContext)
  //const {state, dispatch} = useContext(StateProvider)//useReducer(reducer, { count: 0 });
  var [textdata, setTextData] = useState("")
  return(
    <>
      <label>Yasnita Ismail {state.count}</label>
      <label>Profile {state.profile}</label>
      <label>Data {JSON.stringify(state.data)}</label>
      <label>Data2 {JSON.stringify(state.data2)}</label>
      <button onClick={()=>dispatch({type:'increment'})}>Increment</button>
      <button onClick={()=>dispatch({type:'decrement'})}>Decrement</button>
      <input value={textdata} onChange={(e)=>setTextData(e.target.value)} />
      <button onClick={()=>dispatch({type:'change', text:textdata})}>Change Text</button>
      <button onClick={()=>dispatch({type:'data', data:{'Nama': textdata}})}>Add Data</button>
    </>
  )
}

function ViewData(){
  var {state, dispatch} = useContext(StateContext)
  return(
    <>
      <button>
        Increment: {state.count}
      </button>
      <img src={logo} className="App-logo" alt="logo" />
      <p>
        Edit <code>src/App.js</code> and save to reload.
      </p>
    </>
  )
}

const StateContext =  createContext()

const StateProvider =  (props)=>{
  const [state, dispatch] = useReducer(reducer, { count: 0, data:[], data2:[] });
  return(
    <StateContext.Provider value ={{state, dispatch}}>
      {props.children}
    </StateContext.Provider>
  )
}

function App() {
  return (
    <StateProvider>
    <div className="App">
      <header className="App-header">
        {/* <ViewData/> */}
        <Counter />
        <label></label>
        <ViewCounter/>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        
      </header>
    </div>
    </StateProvider>
  );
}

export default App;
